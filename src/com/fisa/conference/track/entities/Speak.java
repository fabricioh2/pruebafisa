package com.fisa.conference.track.entities;

/**
 * Created by fpacheco on 10/19/19.
 * To read from the xtxt file.
 */
public class Speak {

    private String title;
    private int durationInMins;


    public Speak(String title, int durationInMins) {
        this.title = title;
        this.durationInMins = durationInMins;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDurationInMins() {
        return durationInMins;
    }

    public void setDurationInMins(int durationInMins) {
        this.durationInMins = durationInMins;
    }
}
