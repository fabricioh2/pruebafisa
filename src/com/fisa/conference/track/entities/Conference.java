package com.fisa.conference.track.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fpacheco on 10/19/19.
 */
public class Conference {

    private List<Track> tracks;

    public Conference(List<Track> tracks) {
        this.tracks = tracks;
    }

    public Conference() {
        this.tracks = new ArrayList<>();
    }

    public List<Track> getTracks() {
        return tracks;
    }

    public void setTracks(List<Track> tracks) {
        this.tracks = tracks;
    }
}
