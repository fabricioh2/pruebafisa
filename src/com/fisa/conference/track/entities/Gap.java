package com.fisa.conference.track.entities;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by fpacheco on 10/19/19.
 */
public class Gap {

    private List<Event> events;
    private int minsLeft;
    private LocalTime starTime;

    public Gap(int duration, LocalTime starTime) {
        this.events = new ArrayList<>();
        this.minsLeft = duration;
        this.starTime = starTime;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void addEvent(Event event) {
        minsLeft = minsLeft-event.getDurationInMins();
        this.events.add(event);
    }

    public int getMinsLeft() {
        return minsLeft;
    }

       public LocalTime getStarTime() {
        return starTime;
    }

    public boolean validateEventTime(Speak speak) {
        return minsLeft >= speak.getDurationInMins();
    }
}
