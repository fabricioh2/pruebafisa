package com.fisa.conference.track.entities;

import com.fisa.conference.track.utils.Constants;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

/**
 * Created by fpacheco on 10/19/19.
 */
public class Event {

    private String title;
    private int durationInMins;
    private LocalTime startTime;

    public Event(String title, int durationInMins, LocalTime startTime) {
        this.title = title;
        this.durationInMins = durationInMins;
        this.startTime = startTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDurationInMins() {
        return durationInMins;
    }

    public void setDurationInMins(int durationInMins) {
        this.durationInMins = durationInMins;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    @Override
    public String toString() {
        return this.startTime.format(Constants.TIME_FORMAT)+" "+this.title;
    }
}
