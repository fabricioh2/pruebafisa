package com.fisa.conference.track.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fpacheco on 10/19/19.
 */
public class Track {

    private List<Gap> gaps;
    private int numberTrack;

    public Track(List<Gap> gaps, int numberTrack) {
        this.gaps = gaps;
        this.numberTrack = numberTrack;
    }

    public List<Gap> getGaps() {
        return gaps;
    }


    public int getNumberTrack() {
        return numberTrack;
    }


}
