package com.fisa.conference.track.controllers;

import com.fisa.conference.track.entities.Conference;
import com.fisa.conference.track.entities.Speak;
import com.fisa.conference.track.utils.TxtReader;
import com.fisa.conference.track.utils.Utils;

import java.util.List;

/**
 * Created by fpacheco on 10/19/19.
 */
public class ConferenceTrackManagement {

    public static void main(String[] args) {
        if(args.length==0){
            System.out.println("Debe ingresar la ruta del archivo de Entrada");
            System.exit(-1);
        }
        String file = args[0];
        TxtReader txtReader = new TxtReader();
        List<Speak> speaksfromFile = txtReader.getSpeaksfromFile(file);
        Conference conference = new ConferenceScheduler().processSpeaks(speaksfromFile);
        Utils.printResults(conference);
    }
}
