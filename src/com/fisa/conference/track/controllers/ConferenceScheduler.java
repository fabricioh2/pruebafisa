package com.fisa.conference.track.controllers;

import com.fisa.conference.track.entities.*;
import com.fisa.conference.track.utils.Constants;
import com.fisa.conference.track.utils.SpeakComparator;
import com.fisa.conference.track.utils.Utils;
import jdk.nashorn.internal.objects.annotations.Attribute;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by fpacheco on 10/19/19.
 */
public class ConferenceScheduler {

    public Conference processSpeaks(List<Speak> speaks) {
        Conference conference = new Conference();

        List<Track> tracks = new ArrayList<>();
        int trackNumber = 0;
        while (speaks.size() != 0) {
            List<Gap> conferenceGaps = new ArrayList<>();
            Collections.sort(speaks, new SpeakComparator());

            //LLenar espacios
            Gap amGap = new Gap(Constants.MORNING_TRACK_DURATION_MINUTES, Constants.AM_TRACK_START_TIME);
            //llenar con speaks al gap
            fillGap(amGap, speaks);

            Gap lunchGap = new Gap(Constants.LUNCH_DURATION_MINUTES, Constants.LUNCH_START_TIME);
            lunchGap.addEvent(new Event(Constants.LUNCH_TITLE, Constants.LUNCH_DURATION_MINUTES, Constants.LUNCH_START_TIME));

            Gap pmGap = new Gap(Constants.AFTERNOON_TRACK_DURATION_MINUTES, Constants.PM_TRACK_START_TIME);
            //llenar con speaks
            fillGap(pmGap, speaks);

            Gap networkingEvt = new Gap(Constants.NETWORKING_DURATION_MINUTES, Constants.NETWORKING_START_TIME);
            networkingEvt.addEvent(new Event(Constants.NETWORKING_EVT_TITLE, Constants.NETWORKING_DURATION_MINUTES, Constants.NETWORKING_START_TIME));

            conferenceGaps.add(amGap);
            conferenceGaps.add(lunchGap);
            conferenceGaps.add(pmGap);
            conferenceGaps.add(networkingEvt);
            tracks.add(new Track(conferenceGaps, ++trackNumber));

        }
        conference.setTracks(tracks);
        return conference;
    }

    private Gap fillGap(Gap gap, List<Speak> speaks) {
        LocalTime currentAMTime = gap.getStarTime();

        for (Iterator<Speak> iterator = speaks.iterator(); iterator.hasNext(); ) {
            Speak speak = iterator.next();
            if (gap.validateEventTime(speak)) {
                gap.addEvent(new Event(speak.getTitle(), speak.getDurationInMins(), currentAMTime));
                currentAMTime = Utils.getNextTime(currentAMTime, speak);
                iterator.remove();
            }
        }
        return gap;
    }

}
