package com.fisa.conference.track.utils;

import com.fisa.conference.track.entities.Conference;
import com.fisa.conference.track.entities.Speak;
import com.fisa.conference.track.entities.Track;

import java.time.LocalTime;
import java.util.List;

/**
 * Created by fpacheco on 10/19/19.
 */
public class Utils {

    public static LocalTime getNextTime(LocalTime currentTime, Speak speak){
        return currentTime.plusMinutes(speak.getDurationInMins());
    }

    public static void printResults(Conference conference){
        List<Track> tracks = conference.getTracks();
        for (Track track : tracks) {
            System.out.println("Track " + track.getNumberTrack());
            track.getGaps().stream().forEach(gap ->
                    gap.getEvents().stream().forEach(System.out::println));

        }
    }

}
