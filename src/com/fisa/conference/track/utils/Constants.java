package com.fisa.conference.track.utils;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by fpacheco on 10/19/19.
 */
public class Constants {

    public static final String INPUT_FILE = "input-file.txt";
    public static final DateTimeFormatter TIME_FORMAT = DateTimeFormatter.ofPattern("hh:mm a");

    public static final String LIGHTNING_SPEAK = "lightning";
    public static final String LUNCH_TITLE = "Lunch";
    public static final String NETWORKING_EVT_TITLE = "Networking Event";

    public static final int TOTAL_TRACK_DURATION_MINUTES = 420;
    public static final int MORNING_TRACK_DURATION_MINUTES = 180;
    public static final int AFTERNOON_TRACK_DURATION_MINUTES = 240;

    public static final int LIGHTNING_SPEAK_DURATION_MINUTES = 5;

    public static LocalTime AM_TRACK_START_TIME = LocalTime.of(9,0);
    public static LocalTime LUNCH_START_TIME = LocalTime.of(12, 0);
    public static LocalTime PM_TRACK_START_TIME = LocalTime.of(13, 0);
    public static LocalTime NETWORKING_START_TIME = LocalTime.of(17, 0);

    public static final int LUNCH_DURATION_MINUTES = 60;
    public static final int NETWORKING_DURATION_MINUTES = 60;
}
