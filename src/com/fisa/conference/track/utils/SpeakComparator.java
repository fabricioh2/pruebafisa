package com.fisa.conference.track.utils;

import com.fisa.conference.track.entities.Speak;

import java.util.Comparator;

/**
 * Created by fpacheco on 10/19/19.
 */
public class SpeakComparator implements Comparator<Speak> {
    @Override
    public int compare(Speak sp1, Speak sp2) {
         if(sp1.getDurationInMins()<sp2.getDurationInMins()){
             return 1;
         }else{
             return -1;
         }
    }
}
