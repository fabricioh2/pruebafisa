package com.fisa.conference.track.utils;

import com.fisa.conference.track.entities.Speak;
import com.fisa.conference.track.utils.Constants;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by fpacheco on 10/19/19.
 */
public class TxtReader {
    private static final String NONDIGITPATT = "\\D+";

    public List<Speak> getSpeaksfromFile(String file) {
        List<Speak> speaks = new ArrayList<>();
        String line = "";
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            int duration = 0;
            while ((line = bufferedReader.readLine()) != null) {
                if (line.contains("//") || line.isEmpty())
                    continue;
                String title = line;
                String durationStr = line.substring(line.lastIndexOf(" ") + 1);
                if (Constants.LIGHTNING_SPEAK.equals(durationStr)) {
                    duration = Constants.LIGHTNING_SPEAK_DURATION_MINUTES;
                } else {
                    //Solo numeros
                    durationStr = durationStr.replaceAll(NONDIGITPATT, "");
                    duration = Integer.valueOf(durationStr);
                }
                speaks.add(new Speak(title, duration));
            }

        } catch (FileNotFoundException e) {
            System.err.println("No se encuentra el archivo "+file);
            System.exit(-1);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } catch (NumberFormatException e) {
            System.err.println("No se puede transformar a numero la linea :"+line);
            throw new NumberFormatException(e.getMessage());
        }

        return speaks;
    }
}