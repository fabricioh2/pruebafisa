package test;

import com.fisa.conference.track.entities.Speak;
import com.fisa.conference.track.utils.TxtReader;
import org.junit.Assert;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Admin on 10/19/19.
 */
public class TxtReaderTest {

    TxtReader txtReader = new TxtReader();

    @Test
    public void testGetSpeaksfromFile() {
        List<Speak> speaks = txtReader.getSpeaksfromFile("input-file.txt");
        Assert.assertEquals(19, speaks.size());
    }

    @Test(expected = NumberFormatException.class)
    public void testGetSpeaksfromFileNFE() {
        List<Speak> speaks = txtReader.getSpeaksfromFile("input-invalid.txt");

    }

    @Test(expected = FileNotFoundException.class)
    public void testGetSpeaksfromFileFNE() {
        List<Speak> speaks = txtReader.getSpeaksfromFile("input.txt");

    }
}