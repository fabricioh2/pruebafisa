package test;

import com.fisa.conference.track.controllers.ConferenceTrackManagement;
import org.junit.Test;

import java.io.FileNotFoundException;

import static org.junit.Assert.*;

/**
 * Created by Admin on 10/19/19.
 */
public class ConferenceTrackManagementTest {

    @Test(expected = FileNotFoundException.class)
    public void testMain(){
        String[] an={""};
        new ConferenceTrackManagement().main(an);
    }

}