package test;

import com.fisa.conference.track.entities.Speak;
import com.fisa.conference.track.utils.Utils;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalTime;

import static com.fisa.conference.track.utils.Utils.getNextTime;
import static org.junit.Assert.*;

/**
 * Created by Admin on 10/20/19.
 */
public class UtilsTest {

    Speak speak= null;
    @Before
    public void init (){
        speak= new Speak("Prueba",60);
    }

    @Test
    public void testGetNextTime() {
        LocalTime localTime=Utils.getNextTime(LocalTime.of(10,0),speak);
        assertEquals(11,localTime.getHour());
    }

}