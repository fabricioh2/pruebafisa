# Conference Track

_Ejercicio para prueba técnica Java en Fisa. Fabricio Pacheco_


### Instalación 🔧

_1. Clonar el proyecto en un directorio local con:_ 
```
git clone https://fabricioh2@bitbucket.org/fabricioh2/pruebafisa.git
```
_2. En el directorio del proyecto clonado, compilar y generar el jar._

```
Si utiliza IntelliJ Generar ubicándose en la carpeta principal, ir a Build/Build Artifacts  Build
Si utiliza eclipse, clic derecho en el proyecto, exportar - Jar
```
_3. El archivo 'input-file.txt' contiene la información y formato para poder ejectuar el programa_

_El formato para ingresar nuevos campos es el siguiente:_

```
"Writing Fast Tests Against Enterprise Rails 60min"

Formato:
Titulo XXmin
Titulo, separado por espacios en blanco (Writing Fast Tests Against Enterprise Rails)
Tiempo, con formato XXmin, siendo XX el numero de minutos (60min).
```
_4. Abrir una consola de sistema, ubicarse en la carpeta en donde se encientra el jar generado.(Al generarlo con IntelliJ el archivo se encuebtra en la carpeta out)_

_5. Escribir la siguiente sentencia para correr el programa: (Se debe escribir la ruta del archivo de entrada)_

_Ejemplo:_

```
java -jar ConferenceTrack.jar E:/Datos/input-file.txt
```

_Y se obtiene la salida en consola_

```
Track 1
09:00 AM Ruby on Rails Legacy App Maintenance 60min
10:00 AM Ruby on Rails: Why We Should Move On 60min
11:00 AM Rails Magic 60min
12:00 PM Lunch
01:00 PM Communicating Over Distance 60min
02:00 PM Writing Fast Tests Against Enterprise Rails 60min
03:00 PM Clojure Ate Scala (on my project) 45min
03:45 PM Pair Programming vs Noise 45min
04:30 PM User Interface CSS in Rails Apps 30min
05:00 PM Networking Event
Track 2
09:00 AM Overdoing it in Python 45min
09:45 AM Ruby Errors from Mismatched Gem Versions 45min
10:30 AM Common Ruby Errors 45min
11:15 AM Accounting-Driven Development 45min
12:00 PM Lunch
01:00 PM Lua for the Masses 30min
01:30 PM Woah 30min
02:00 PM Sit Down and Write 30min
02:30 PM Programming in the Boondocks of Seattle 30min
03:00 PM Ruby vs. Clojure for Back-End Development 30min
03:30 PM A World Without HackerNews 30min
04:00 PM Rails for Python Developers lightning
05:00 PM Networking Event
```
